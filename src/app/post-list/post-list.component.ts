import { Component, DoCheck } from '@angular/core';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements DoCheck {

  posts: any[];

  constructor(private postService: PostService) {}

  ngDoCheck() {
    this.posts = this.postService.filteredposts;
  }

}
