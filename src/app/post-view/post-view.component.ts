import { Component, OnInit } from '@angular/core';
import { PostService } from '../post.service';
import { Post } from '../model/Post';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
export class PostViewComponent implements OnInit {

  constructor(private postService: PostService){}

  title = 'Posts';

  posts: Post[];

  ngOnInit() {
    this.posts = this.postService.posts;
  }

}
