import { Component, OnInit, Input } from '@angular/core';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() title: string;
  @Input() content: string;
  @Input() created: string;
  @Input() loveIts: number;
  @Input() index: number;

  constructor(private postService: PostService) { }

  ngOnInit() {
  }

  LoveIts(){
    ++this.loveIts;
  }

  NotLoveIts(){
    --this.loveIts;
  }

  removePost(self){
    console.log(self);
    this.postService.remove(this.index);
  }

}
