import { Component, OnInit, Input } from '@angular/core';
import { PostService } from '../post.service';
@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  constructor(private postService: PostService) {}
  filteredposts: any[];
  currentFilter: string;
  @Input() posts: any[];

  filterPost(search){
    this.currentFilter = search.value;
    this.filteredposts = this.posts.filter((element) => this.goodFilter(element));
    this.postService.filterPost(this.filteredposts);
    return;
  }

  goodFilter(element){
    // var RegEx = new RegExp(element.title)
    // return this.currentFilter
    return element.title == this.currentFilter;
  }

  ngOnInit() {
  }

}
