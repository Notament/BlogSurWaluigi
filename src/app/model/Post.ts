export class Post{

    loveIts: number;
    created: Date;
    title: string;
    content: string;


    constructor(title, content){
        this.title = title;
        this.content = content;
        this.created = new Date();
        this.loveIts = 0;
    }
}