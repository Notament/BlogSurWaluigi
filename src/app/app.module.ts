import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostListItemComponent } from './post-list-item/post-list-item.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { PostService } from './post.service';
import { PostViewComponent } from './post-view/post-view.component';
import { AddPostComponent } from './add-post/add-post.component';
import { NavComponent } from './nav/nav.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';


const appRoutes: Routes = [
  { path: 'posts', component: PostViewComponent },
  { path: 'new', component: AddPostComponent },
  { path: '', component: PostViewComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    PostListItemComponent,
    SearchBarComponent,
    PostViewComponent,
    AddPostComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    PostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
